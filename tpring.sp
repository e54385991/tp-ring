#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <dhooks>
#include <outputinfo>
#include <cstrike>
#include <colors>

#pragma semicolon 1
#pragma newdecls required

public Plugin myinfo =
{
    name = "Teleport Ring",
    author = "KiceQishi",
    description = "Show Teleport pos and the time",
    version = "0.1",
    url = ""
}

int g_HaloSprite;
int g_BeamSprite;
// int g_BombRing;

#define VMT_LASERBEAM "materials/sprites/laserbeam.vmt"
// #define VMT_BOMBRING "materials/sprites/bomb_planted_ring.vmt"
#define VMT_HALO "materials/sprites/halo.vmt"

int RingColor[][4] = {
    {255, 128, 0, 255},
    {255, 255, 255, 255},
    {255, 75, 75, 255},
    {75, 75, 255, 255},
};

char classname[][32] = {
    "logic_auto", 
    "logic_case", "logic_case", "logic_case", "logic_case", 
    "logic_case", "logic_case", "logic_case", "logic_case", 
    "logic_case", "logic_case", "logic_case", "logic_case", 
    "logic_case", "logic_case", "logic_case", 
    "trigger_once", "trigger_multiple", 
    // "trigger_teleport", "trigger_teleport", "trigger_teleport", "trigger_teleport",
    "func_breakable", "func_movelinear", "func_movelinear", "func_button",
    "math_counter", "math_counter", "path_track",
};

char outputname[][32] = {
    "OnMapSpawn",
    "OnCase01", "OnCase02", "OnCase03", "OnCase04",
    "OnCase05", "OnCase06", "OnCase07", "OnCase08",
    "OnCase09", "OnCase10", "OnCase11", "OnCase12",
    "OnCase13", "OnCase15", "OnCase16", 
    "OnStartTouch", "OnStartTouch", 
    // "OnUser1", "OnUser2", "OnUser3", "OnUser4",
    "OnBreak", "OnFullyOpen", "OnFullyClosed", "OnPressed", 
    "OnHitMax", "OnHitMin", "OnPass",
};

char eventList[][32] = {
    "m_OnMapSpawn",

    "m_OnStartTouch",
    "m_OnTrigger",
    "m_OnPressed",
    "m_OnBreak",
    
    "m_OnUser1", "m_OnUser2", "m_OnUser3", "m_OnUser4",

    "m_OnCase[0]", "m_OnCase[1]", "m_OnCase[2]", "m_OnCase[3]",
    "m_OnCase[4]", "m_OnCase[5]", "m_OnCase[6]", "m_OnCase[7]",
    "m_OnCase[8]", "m_OnCase[9]", "m_OnCase[10]", "m_OnCase[11]",
    "m_OnCase[12]", "m_OnCase[13]", "m_OnCase[14]", "m_OnCase[15]",

    "m_OnFullyOpen", "m_OnFullyClosed",

    "m_OnHitMax", "m_OnHitMin",
    
    "m_OnPass",
};

ArrayList HookClassName;
ArrayList HookOutputName;

ArrayList OutputList;

ArrayList ActivatedList;

float g_roundEndTime = -1.0;

bool g_bEnable = true;
bool g_bTime = true;
bool g_bRing = true;
bool g_bRingOnce = true;
bool g_bVScriptHook = true;

// static void DoEntFire( const char *pszTarget, const char *pszAction, const char *pszValue, float delay, HSCRIPT hActivator, HSCRIPT hCaller )
Handle hDoEntFireDetour = INVALID_HANDLE;

void UnHookOutput()
{
    for (int i = 0; i < HookClassName.Length; ++i) {
        char class[32], output[32];
        HookClassName.GetString(i, class, 32);
        HookOutputName.GetString(i, output, 32);

        if (strlen(output) == 0) {
            PrintToServer("OutputName[%s] is empty", class);
            continue;
        }

        UnhookEntityOutput(class, output, OnEntityOutput);
    }

    DHookDisableDetour(hDoEntFireDetour, false, OnDoEntFire);
}

void HookOutput()
{
    for (int i = 0; i < HookClassName.Length; ++i) {
        char class[32], output[32];
        HookClassName.GetString(i, class, 32);
        HookOutputName.GetString(i, output, 32);

        if (strlen(output) == 0) {
            PrintToServer("OutputName[%s] is empty", class);
            continue;
        }

        HookEntityOutput(class, output, OnEntityOutput);
    }

    if (g_bVScriptHook) {
        if (!DHookEnableDetour(hDoEntFireDetour, false, OnDoEntFire)) {
            SetFailState("Failed to detour DoEntFire.");
        }

        PrintToServer("[TP] handle 0x%X", view_as<int>(hDoEntFireDetour));
    }
}

void BuildList()
{
    if (sizeof(classname) != sizeof(outputname)) {
        PrintToServer("size of classname and outputname unmatch");
        return;
    }

    HookClassName = new ArrayList(32, 0);
    HookOutputName = new ArrayList(32, 0);
    for (int i = 0; i < sizeof(classname); ++i) {
        HookClassName.PushString(classname[i]);
        HookOutputName.PushString(outputname[i]);
    }
    PrintToServer("HookEntityOutput size: %d", HookClassName.Length);

    OutputList = new ArrayList(32, 0);
    for (int i = 0; i < sizeof(eventList); ++i) {
        OutputList.PushString(eventList[i]);
    }
    PrintToServer("OutputSearch size: %d", OutputList.Length);
}

void PrepareMaterialsAndModels()
{
    g_BeamSprite = PrecacheModel(VMT_LASERBEAM);
    g_HaloSprite = PrecacheModel(VMT_HALO);
    // g_BombRing = PrecacheModel(VMT_BOMBRING);
}

public void OnPluginStart()
{
    RegAdminCmd("sm_tpring", CmdTPRing, ADMFLAG_BAN);
    PrepareMaterialsAndModels();

    Handle hGameConf = INVALID_HANDLE;
    hGameConf = LoadGameConfigFile("vscriptserver.games");
	if (hGameConf == INVALID_HANDLE) {
		SetFailState("Can't find vscriptserver.games.txt gamedata.");
	}

    hDoEntFireDetour = DHookCreateFromConf(hGameConf, "DoEntFire");
    if (hDoEntFireDetour == INVALID_HANDLE) {
        SetFailState("Failed to setup detour");
    }

    BuildList();
    HookOutput();

    HookEvent("round_start", OnRoundStart);

    ActivatedList = new ArrayList();

    CloseHandle(hGameConf);
}

public void OnPluginEnd()
{
    UnHookOutput();
    UnhookEvent("round_start", OnRoundStart);
}

public void OnMapStart()
{
    PrepareMaterialsAndModels();
}

stock float GetRoundTimeLeft()
{
    return (g_roundEndTime - GetGameTime());
}

public Action OnRoundStart(Handle event, const char[] name, bool dontBroadcast)
{
    ActivatedList.Clear();

    Handle freezeTime = FindConVar("mp_freezetime");
    g_roundEndTime = GetGameTime() + GetConVarInt(freezeTime) + GameRules_GetProp("m_iRoundTime");
}

public Action CmdTPRing(int client, int args)
{
    if (args == 0) {
        ReplyToCommand(client, "Teleport Ring is %s", g_bEnable ? "Enabled" : "Disabled");
        return Plugin_Handled;
    }

    char cmd[8];
    GetCmdArg(1, cmd, sizeof(cmd));

    if (StrEqual(cmd, "toggle")) {
        g_bEnable = !g_bEnable;
        ReplyToCommand(client, "Teleport Ring Plugin is %s", g_bEnable ? "Enabled" : "Disabled");
    } else if (StrEqual(cmd, "hook")) {
        HookOutput();
        ReplyToCommand(client, "Teleport Ring was hooked");

    } else if (StrEqual(cmd, "unhook")) {
        UnHookOutput();
        ReplyToCommand(client, "Teleport Ring was unhooked");

    } else if (StrEqual(cmd, "hookvscript")) {
        g_bVScriptHook = !g_bVScriptHook;
        if (g_bVScriptHook) {
            if (!DHookEnableDetour(hDoEntFireDetour, false, OnDoEntFire)) {
                SetFailState("Failed to detour DoEntFire.");
            }

            ReplyToCommand(client, "[TP] handle 0x%X", view_as<int>(hDoEntFireDetour));
        } else {
            DHookDisableDetour(hDoEntFireDetour, false, OnDoEntFire);
        }

        ReplyToCommand(client, "VScript hook is", g_bVScriptHook ? "Enabled" : "Disabled");

    } else if (StrEqual(cmd, "reset")) {
        UnHookOutput();
        HookOutput();
        ReplyToCommand(client, "Teleport Ring was reseted");

    } else if (StrEqual(cmd, "time")) {
        g_bTime = !g_bTime;
        ReplyToCommand(client, "Teleport Time is %s", g_bTime ? "Enabled" : "Disabled");

    } else if (StrEqual(cmd, "ring")) {
        g_bRing = !g_bRing;
        ReplyToCommand(client, "Teleport Ring is %s", g_bRing ? "Enabled" : "Disabled");

    } else if (StrEqual(cmd, "ringonce")) {
        g_bRingOnce = !g_bRingOnce;
        ReplyToCommand(client, "Teleport Ring Once per round is %s", g_bRingOnce ? "Enabled" : "Disabled");

    } else if (StrEqual(cmd, "list")) {
        ReplyToCommand(client, "[TP] HookEntityOutput List");
        for (int i = 0; i < HookClassName.Length; ++i) {
            char class[32], output[32];
            HookClassName.GetString(i, class, 32);
            HookOutputName.GetString(i, output, 32);
            
            ReplyToCommand(client, "[TP] id: %d %s => %s", i, class, output);
        }
        ReplyToCommand(client, "[TP] HookEntityOutput List End");

        ReplyToCommand(client, "[TP] Output search List");
        for (int i = 0; i < OutputList.Length; ++i) {
            char output[32];
            OutputList.GetString(i, output, 32);
            
            ReplyToCommand(client, "[TP] id: %d %s", i, output);
        }
        ReplyToCommand(client, "[TP] Output search List End");

    } else if (StrEqual(cmd, "addhook")) {
        if (args < 3) {
            ReplyToCommand(client, "[TP] Hook entity output. Usage: sm_tpring addhook classname output (e.g. sm_tpring addhook trigger_once OnTrigger)");
            return Plugin_Handled;
        }
        char class[32], output[32];
        GetCmdArg(2, class, 32);
        GetCmdArg(3, output, 32);

        HookClassName.PushString(class);
        HookOutputName.PushString(output);

        HookEntityOutput(class, output, OnEntityOutput);

        ReplyToCommand(client, "[TP] Hook %s %s successful. id=%d", class, output, HookClassName.Length - 1);

    } else if (StrEqual(cmd, "addoutput")) {
        if (args < 2) {
            ReplyToCommand(client, "[TP] Check entity output list for teleport data. Usage: sm_tpring addoutput output (e.g. m_OnTrigger)");
            return Plugin_Handled;
        }
        char output[32];
        GetCmdArg(2, output, 32);

        OutputList.PushString(output);
        ReplyToCommand(client, "[TP] Add %s to list successful. id=%d", output, OutputList.Length - 1);

    } else if (StrEqual(cmd, "delhook")) {
        if (args == 2) {
            char szId[8];
            GetCmdArg(2, szId, 8);

            for (int i = 0; i < strlen(szId); ++i) {
                if (!IsCharNumeric(szId[i])) {
                    ReplyToCommand(client, "[TP] id must be number. but got %s", szId);
                    return Plugin_Handled;
                }
            }

            int id = StringToInt(szId);
            if (id >= HookClassName.Length) {
                ReplyToCommand(client, "[TP] Invaild id, got %d", id);
                return Plugin_Handled;
            }

            char class[32], output[32];
            HookClassName.GetString(id, class, 32);
            HookOutputName.GetString(id, output, 32);

            UnhookEntityOutput(class, output, OnEntityOutput);

            HookClassName.Erase(id);
            HookOutputName.Erase(id);

            ReplyToCommand(client, "[TP] id: %d %s => %s remove successfully", id, class, output);
            return Plugin_Handled;
        } else if (args == 3) {
            char tclass[32], toutput[32];
            GetCmdArg(2, tclass, 32);
            GetCmdArg(3, toutput, 32);

            for (int i = 0; i < HookClassName.Length; ++i) {
                char class[32], output[32];
                HookClassName.GetString(i, class, 32);
                HookOutputName.GetString(i, output, 32);
                
                if (StrEqual(tclass, class, false) && StrEqual(toutput, output, false)) {
                    UnhookEntityOutput(class, output, OnEntityOutput);
                    ReplyToCommand(client, "[TP] id: %d %s => %s remove successfully", i, class, output);
                    return Plugin_Handled;
                }
            }

            ReplyToCommand(client, "[TP] Cannot find hook class: %s with %", tclass, toutput);
            return Plugin_Handled;
        }

        ReplyToCommand(client, "[TP] Hook entity output. Usage: sm_tpring delhook classname output or sm_tpring delhook id");

    } else if (StrEqual(cmd, "deloutput")) {
        if (args < 2) {
            ReplyToCommand(client, "[TP] Remove an output from check list. Usage: sm_tpring deloutput outputname or sm_tpring deloutput id");
            return Plugin_Handled;
        }

        char toutput[32];
        GetCmdArg(2, toutput, 32);

        for (int i = 0; i < OutputList.Length; ++i) {
            char output[32];
            OutputList.GetString(i, output, 32);
            
            if (StrEqual(toutput, output, false)) {
                ReplyToCommand(client, "[TP] id=%d, %s remove from list successfully.", output);
                return Plugin_Handled;
            }
        }

        ReplyToCommand(client, "[TP] Cannot find %s from list", toutput);
    }
    
    return Plugin_Handled;
}

public MRESReturn OnDoEntFire(Handle hParams)
{
    // char pszAction[32];
    // DHookGetParamString(hParams, 2, pszAction, sizeof(pszAction));
    // if (StrEqual(pszAction, "Enable", false)) {
    //     return MRES_Ignored;
    // }

    char pszTarget[32];
    DHookGetParamString(hParams, 1, pszTarget, sizeof(pszTarget));
    int entity = FindEntityByName(pszTarget);
    if (entity == INVALID_ENT_REFERENCE) {
        return MRES_Ignored;
    }

    char class[32];
    GetEntityClassname(entity, class, 32);
    if (!StrEqual("trigger_teleport", class)) {
        return MRES_Ignored;
    }

    float delay = DHookGetParam(hParams, 4);
    PrintToServer("[OnDoEntFire] teleport:%s(%d) delay:%.2fs", pszTarget, entity, delay);
    OnTeleportEnable(entity, delay);

    return MRES_Ignored;
}

public Action OnEntityOutput(const char[] outputName, int entity, int activator, float delay)
{
    if (!g_bEnable) {
        return Plugin_Continue;
    }

    for (int i = 0; i < OutputList.Length; ++i) {
        char output[32];
        OutputList.GetString(i, output, 32);

        if (FindTeleport(entity, output)) {
            break;
        }
    }

    return Plugin_Continue;
}

public bool OnTeleportEnable(int entTeleport, float delay)
{
    int bDisabled = GetEntProp(entTeleport, Prop_Data, "m_bDisabled", 1);
    if (bDisabled == 0) {
        // if the teleport is already enabled, no need to tell player
        return true;
    }

    char remote[32];
    GetEntPropString(entTeleport, Prop_Data, "m_target", remote, 32);
    int remoteEnt = FindEntityByName(remote);
    if (remoteEnt == INVALID_ENT_REFERENCE) {
        PrintToServer("Cannot find teleport target. %d => %s", entTeleport, remote);
        return false;
    }

    float position[3];
    GetEntPropVector(remoteEnt, Prop_Send, "m_vecOrigin", position);

    char tpOutput[][32] = {
        "m_OnUser1", "m_OnUser2", "m_OnUser3", "m_OnUser4",
    };

    for (int i = 0; i < sizeof(tpOutput); ++i) {
        int tcount = GetOutputActionCount(entTeleport, tpOutput[i]);
        // if tp set dest before enable
        for (int j = 0; j < tcount; ++j) {
            char ttarget[32], tinput[32];
            GetOutputActionTarget(entTeleport, tpOutput[i], j, ttarget, 32);
            GetOutputActionTargetInput(entTeleport, tpOutput[i], j, tinput, 32);

            // make sure it is targetting on dest and adding the output
            if (StrEqual(remote, ttarget) && StrEqual("AddOutput", tinput, false)) {
                char tparam[64];
                GetOutputActionParameter(entTeleport, tpOutput[i], j, tparam, 64);

                char param[4][16];
                int result = ExplodeString(tparam, " ", param, 4, 16, false);
                if (result == 4 && StrEqual("origin", param[0], false)) {
                    for (int k = 0; k < 3; ++k) {
                        position[k] = StringToFloat(param[k+1]);
                    }

                    PrintToServer("[TP] Reset teleport(%d) position to %.2f %.2f %.2f ", entTeleport, position[0], position[1], position[2]);
                    break;
                }
            }
        }
    }

    OnTeleport(entTeleport, position, delay);
    return true;
}

public bool FindTeleport(int entity, const char[] output)
{
    int nTp = 0;

    int count = GetOutputActionCount(entity, output);
    for (int i = 0; i < count; ++i) {
        char target[32], input[32], param[64];
        GetOutputActionTarget(entity, output, i, target, 32);
        GetOutputActionTargetInput(entity, output, i, input, 32);
        GetOutputActionParameter(entity, output, i, param, 64);

        float outputdelay = GetOutputActionDelay(entity, output, i);

        int entTeleport = FindEntityByName(target);
        if (entTeleport == INVALID_ENT_REFERENCE) {
            continue;
        }

        char tclass[32];
        GetEntityClassname(entTeleport, tclass, 32);
        if (!StrEqual("trigger_teleport", tclass) || !StrEqual("Enable", input, false)) {
            continue;
        }

        int bDisabled = GetEntProp(entTeleport, Prop_Data, "m_bDisabled", 1);
        if (bDisabled == 0) {
            // if the teleport is already enabled, no need to tell player
            continue;
        }

        PrintToServer("Ouput[%s] => Target:%s Input:%s Param:%s delay:%.2f", output, target, input, param, outputdelay);
        
        // Get dest
        char remote[32];
        GetEntPropString(entTeleport, Prop_Data, "m_target", remote, 32);
        int remoteEnt = FindEntityByName(remote);
        if (remoteEnt == INVALID_ENT_REFERENCE) {
            continue;
        }

        float position[3];
        GetEntPropVector(remoteEnt, Prop_Send, "m_vecOrigin", position);

        OnTeleport(entTeleport, position, outputdelay);
        ++nTp;
    }

    return nTp > 0;
}

public void OnTeleport(int entTeleport, float position[3], float delay) 
{
    // can have 5 filter, but we only check the first one
    int filterTeam = 0;
    do {
        char filterName[32];
        if (GetEntPropString(entTeleport, Prop_Data, "m_iFilterName", filterName, 32, 0) == 0) {
            break;
        }

        int entFilter = FindEntityByName(filterName);
        if (entFilter == INVALID_ENT_REFERENCE) {
            break;
        }

        char fclass[32];
        GetEntityClassname(entFilter, fclass, 32);
        if (!StrEqual("filter_activator_team", fclass)) {
            break;
        }

        filterTeam = GetEntProp(entFilter, Prop_Data, "m_iFilterTeam", 4);
    } while(false);

    PrintToConsoleAll("[TP] team: %d pos: %.2f %.2f %.2f in %.2fs", filterTeam, position[0], position[1], position[2], delay);

    for (int i = ActivatedList.Length - 1; i >= 0 ; --i) {
        if (entTeleport == ActivatedList.Get(i)) {
            return;
        }
    }

    ActivatedList.Push(entTeleport);

    if (g_bTime) {
        if (delay <= 1.0) {
            delay = 0.0;
            CPrintToChatAll("[TP] {orange} A teleport was triggered.");
        } else {
            char team[32] = "{orange} All Players {default}";
            if (filterTeam == CS_TEAM_T) {
                team = "{lightred} Zombies {default}";
            } else if (filterTeam == CS_TEAM_CT) {
                team = "{blue} Humans {default}";
            }

            float timeLeft = GetRoundTimeLeft() - delay;
            int m = RoundToFloor(timeLeft / 60.0);
            int s = RoundToNearest(timeLeft - m * 60.0);
            CPrintToChatAll("[TP] %s teleport will be at {green}%02d:%02d", team, m, s);
        }
    }

    if (g_bRing) {
        if (delay > 30.0) {
            GlowSprite(position, delay - 25.0, "1.0", "17 40 255", "9");
            GlowSprite(position, delay - 25.0, "0.5", "255 255 255", "9");
            GlowSprite(position, delay - 25.0, "0.5", "255 17 17", "9");
        }

        DataPack pack;
        CreateDataTimer(delay - 25.0, Timer_TeleportRing, pack);

        pack.WriteFloat(position[0]);
        pack.WriteFloat(position[1]);
        pack.WriteFloat(position[2]);

        pack.WriteFloat(delay);
        pack.WriteFloat(GetGameTime());
        pack.WriteCell(filterTeam);
    }
}

stock void GlowSprite(const float position[3], float life, const char[] scale, const char[] color, const char[] rendermode)
{
    int sprite = CreateEntityByName("env_sprite");
    if (IsValidEntity(sprite)) {
        return;
    }
    
    DispatchKeyValueVector(sprite, "origin", position);
    DispatchKeyValue(sprite, "spawnflags", "0");
    DispatchKeyValue(sprite, "scale", scale);
    DispatchKeyValue(sprite, "rendermode", rendermode);
    DispatchKeyValue(sprite, "rendercolor", color);
    DispatchKeyValue(sprite, "renderamt", "255");
    DispatchKeyValue(sprite, "model", "sprites/glow01.spr");
    DispatchKeyValue(sprite, "fademindist", "-1");
    DispatchSpawn(sprite);
    AcceptEntityInput(sprite, "ShowSprite");
    CreateTimer(life, Timer_KillEntity, sprite);
}

public Action Timer_KillEntity(Handle timer, any enitiy)
{
    if (IsValidEntity(entity)) {
        AcceptEntityInput(enitiy, "Kill");
    }
    
    return Plugin_Stop;
}

public Action Timer_TeleportRing(Handle timer, Handle pack)
{
    float position[3], delay, triggerTime;
    ResetPack(pack);

    position[0] = ReadPackFloat(pack);
    position[1] = ReadPackFloat(pack);
    position[2] = ReadPackFloat(pack);

    delay = ReadPackFloat(pack);
    triggerTime = ReadPackFloat(pack);

    float tdelay = delay - (GetGameTime() - triggerTime);

    int filterTeam = ReadPackCell(pack);
    if (filterTeam > sizeof(RingColor) || filterTeam < 0) {
        filterTeam = 0;
    }

    TE_SetupBeamRingPoint(position, 120.0, delay > 1.0 ? 0.0 : 120.0, g_BeamSprite, g_HaloSprite, 0, 0, delay > 1.0 ? tdelay : 25.0, 5.0, 1.0, RingColor[filterTeam], 0, 0);
    TE_SendToAll();

    return Plugin_Stop;
}

stock int FindEntityByName(const char[] name)
{
    int maxEntities = GetMaxEntities() * 2;

    char buf[32];
    for (int entity = 0; entity < maxEntities; entity++) {
        if (!IsValidEntity(entity)) {
            continue;
        }

        GetEntPropString(entity, Prop_Data, "m_iName", buf, 32);
        if (StrEqual(name, buf)) {
            return entity;
        }
    }

    return INVALID_ENT_REFERENCE;
}
